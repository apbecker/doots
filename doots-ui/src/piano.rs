use std::collections::HashSet;

use doots::theory::{Chord, NamedChord, Note};
use sauron::{html, Node};

pub struct App {
  notes: HashSet<Note>,
}

impl App {
  pub fn new() -> Self {
    Self { notes: HashSet::new() }
  }

  pub fn update(&mut self, msg: Msg)
  where
    Self: Sized + 'static,
  {
    match msg {
      Msg::ClickNote(note) => {
        if self.notes.contains(&note) {
          self.notes.remove(&note);
        } else {
          self.notes.insert(note);
        }
      }
    }
  }

  pub fn view(&self) -> Node<Msg> {
    html::div([], [self.view_note_keys(), self.view_known_chords()])
  }

  fn view_known_chords(&self) -> Node<Msg> {
    let known_chords = Chord::search(&self.notes);

    html::p([], [html::text("chords: "), self.view_known_chord(&known_chords)])
  }

  fn view_known_chord(&self, known_chords: &[(Note, NamedChord)]) -> Node<Msg> {
    let mut lis = vec![];

    for (root, known_chord) in known_chords {
      lis.push(html::li(
        [],
        [
          html::text(root),
          html::text(known_chord.suffix()),
          html::sub([], [html::text(known_chord.intervals())]),
        ],
      ));
    }

    html::ul([], lis)
  }

  fn view_note_keys(&self) -> Node<Msg> {
    html::div(
      [sauron::class("piano")],
      [
        // naturals
        self.view_note(Note::C, &["note-c", "natural"]),
        self.view_note(Note::D, &["note-d", "natural"]),
        self.view_note(Note::E, &["note-e", "natural"]),
        self.view_note(Note::F, &["note-f", "natural"]),
        self.view_note(Note::G, &["note-g", "natural"]),
        self.view_note(Note::A, &["note-a", "natural"]),
        self.view_note(Note::B, &["note-b", "natural"]),
        // accidentals
        self.view_note(Note::CSharp, &["note-cs", "accidental"]),
        self.view_note(Note::DSharp, &["note-ds", "accidental"]),
        self.view_note(Note::FSharp, &["note-fs", "accidental"]),
        self.view_note(Note::GSharp, &["note-gs", "accidental"]),
        self.view_note(Note::ASharp, &["note-as", "accidental"]),
      ],
    )
  }

  fn view_note(&self, note: Note, classes: &[&'static str]) -> Node<Msg> {
    let mut classes = classes.to_vec();

    if self.notes.contains(&note) {
      classes.push("selected");
    }

    let msg = move |_| Msg::ClickNote(note);

    html::div(
      [sauron::classes(classes)],
      [html::button([sauron::on_click(msg)], [html::text(note)])],
    )
  }
}

#[derive(Clone, Copy, Debug)]
pub enum Msg {
  ClickNote(Note),
}
