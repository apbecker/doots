use std::collections::HashMap;

use doots::guitar::Tuning;
use doots::theory::{Note, Spn};
use sauron::html;

#[macro_export]
macro_rules! log {
  ( $msg:expr ) => {
    sauron::web_sys::console::log_1(&format!($msg).into());
  };
}

pub struct App {
  fretboard: Fretboard,
}

impl App {
  pub fn new() -> Self {
    Self {
      fretboard: Fretboard::new(Tuning::standard()),
    }
  }

  pub fn update(&mut self, msg: Msg) {
    match msg {
      Msg::ClearNotes => {
        self.fretboard.unselect_all();
      }
      Msg::ToggleNote(location) => {
        self.fretboard.toggle(location);
      }
      Msg::Flatten(n) => {
        self.fretboard.flatten(n);
      }
      Msg::Sharpen(n) => {
        self.fretboard.sharpen(n);
      }
    }
  }

  pub fn view(&self) -> sauron::Node<Msg> {
    let mut fretboard = vec![];
    fretboard.extend(self.view_inlays());
    fretboard.extend(self.view_strings());
    fretboard.extend(self.view_inlays());

    html::div(
      [],
      [
        html::div([sauron::class("fretboard")], fretboard),
        html::button([sauron::on_click(move |_| Msg::ClearNotes)], [html::text("Clear Notes")]),
        {
          let notes = self
            .fretboard
            .selected()
            .map(|e| e.to_string())
            .collect::<Vec<_>>()
            .join(", ");

          html::p([], [html::text("Notes: "), html::text(notes)])
        },
      ],
    )
  }

  fn view_strings(&self) -> Vec<sauron::Node<Msg>> {
    let mut strings = vec![];

    for n in self.fretboard.strings() {
      strings.extend(self.view_string(n));
    }

    strings
  }

  fn view_string(&self, n: usize) -> Vec<sauron::Node<Msg>> {
    // TODO: enharmonic naming of selected notes

    let intervals = self.fretboard.intervals();
    let mut notes = vec![];

    notes.push(self.view_tune(n));

    for fret in self.fretboard.frets() {
      let location = FretboardLocation { string: n, fret };
      let spn = self.fretboard.spn(n, fret);
      let intervals = &intervals[&spn.note()];

      let text = {
        let mut attrs = vec![];

        let children = if self.fretboard.is_selected(spn.note()) {
          attrs.push(sauron::class("note"));
          self.view_spn(spn).to_vec()
        } else {
          vec![]
        };

        if let Some(n) = &intervals.note {
          attrs.push(sauron::class("int"));
          attrs.push(sauron::class(format!("int-{n}")));
        }

        html::span(attrs, children)
      };

      notes.push(html::div(
        [sauron::class("fret"), sauron::on_click(move |_| Msg::ToggleNote(location))],
        [view_interval(intervals.below), text, view_interval(intervals.above)],
      ));
    }

    notes
  }

  fn view_spn(&self, spn: Spn) -> [sauron::Node<Msg>; 2] {
    let note = html::text(spn.note());
    let octave = html::text(spn.octave());

    [note, html::sub([], [octave])]
  }

  fn view_tune(&self, n: usize) -> sauron::Node<Msg> {
    let spn = self.fretboard.spn(n, 0);

    html::div(
      [sauron::class("tune")],
      [
        html::button([sauron::on_click(move |_| Msg::Flatten(n))], [html::text("♭")]),
        html::span([], self.view_spn(spn)),
        html::button([sauron::on_click(move |_| Msg::Sharpen(n))], [html::text("♯")]),
      ],
    )
  }

  fn view_inlays(&self) -> Vec<sauron::Node<Msg>> {
    vec![
      html::span([], []),                 // open name
      html::span([], []),                 // 0
      html::span([], []),                 // 1
      html::span([], []),                 // 2
      html::span([], [html::text("●")]),  // 3
      html::span([], []),                 // 4
      html::span([], [html::text("●")]),  // 5
      html::span([], []),                 // 6
      html::span([], [html::text("●")]),  // 7
      html::span([], []),                 // 8
      html::span([], [html::text("●")]),  // 9
      html::span([], []),                 // 10
      html::span([], []),                 // 11
      html::span([], [html::text("●●")]), // 12
    ]
  }
}

fn view_interval(interval: Option<u8>) -> sauron::Node<Msg> {
  if let Some(n) = interval {
    html::span([sauron::classes(["int".to_string(), format!("int-{n}")])], [])
  } else {
    html::span([], [])
  }
}

#[derive(Clone, Copy, Debug)]
pub enum Msg {
  // Frets
  ToggleNote(FretboardLocation),
  ClearNotes,
  // Strings
  Flatten(usize),
  Sharpen(usize),
}

pub struct NoteIntervals {
  above: Option<u8>,
  below: Option<u8>,
  note: Option<u8>,
}

#[derive(Clone, Copy, Debug)]
pub struct FretboardLocation {
  string: usize,
  fret: u8,
}

pub struct Fretboard {
  selected: [bool; Note::MEMBERS],
  strings: Vec<Spn>,
}

impl Fretboard {
  pub fn new(tuning: Tuning) -> Self {
    Self {
      selected: Default::default(),
      strings: tuning.open_strings().collect(),
    }
  }

  fn sharpen(&mut self, n: usize) {
    self.strings[n] = self.strings[n].add(1);
  }

  fn flatten(&mut self, n: usize) {
    self.strings[n] = self.strings[n].sub(1);
  }

  fn spn(&self, n: usize, fret: u8) -> Spn {
    self.strings[n].add(fret)
  }

  pub fn strings(&self) -> impl Iterator<Item = usize> {
    let n = self.strings.len();
    (0..n).rev()
  }

  pub fn frets(&self) -> impl Iterator<Item = u8> {
    let n = Note::MEMBERS as u8;
    0..=n
  }

  pub fn selected(&self) -> impl Iterator<Item = Note> + '_ {
    self
      .selected
      .iter()
      .enumerate()
      .filter(|(_, &n)| n)
      .map(|(n, _)| Note::from_semitones(n as u8))
  }

  pub fn is_selected(&self, note: Note) -> bool {
    self.selected[note as usize]
  }

  pub fn unselect_all(&mut self) {
    for n in &mut self.selected {
      *n = false;
    }
  }

  pub fn toggle(&mut self, location: FretboardLocation) {
    let string = location.string;
    let fret = location.fret;
    let note = self.strings[string].add(fret).note();

    self.selected[note as usize] = !self.selected[note as usize];
  }

  pub fn as_vec(&self) -> Vec<Note> {
    let mut notes = vec![];

    for note in Note::ALL {
      if self.is_selected(note) {
        notes.push(note);
      }
    }

    notes.sort();
    notes
  }

  pub fn intervals(&self) -> HashMap<Note, NoteIntervals> {
    let mut intervals = HashMap::new();

    for note in Note::ALL {
      intervals.insert(
        note,
        NoteIntervals {
          above: None,
          below: None,
          note: None,
        },
      );
    }

    let iter = self.as_vec().into_iter();

    for (note, next_note) in PairwiseIter::new(iter) {
      let k = note.distance_up_to(next_note);
      let k = if k == 0 { 12 } else { k };

      {
        let note_classes = intervals.get_mut(&note).unwrap();
        note_classes.above = Some(k);
      }

      for n in 1..k {
        let note = note.move_up(n);
        let note_classes = intervals.get_mut(&note).unwrap();

        note_classes.above = Some(k);
        note_classes.below = Some(k);
      }

      {
        let note_classes = &mut intervals.get_mut(&next_note).unwrap();
        note_classes.below = Some(k);
      }
    }

    intervals
  }
}

pub struct PairwiseIter<T: Iterator> {
  first: Option<T::Item>,
  item: Option<T::Item>,
  next: Option<T::Item>,
  iter: T,
}

impl<T> PairwiseIter<T>
where
  T: Iterator,
  T::Item: Clone + Copy,
{
  pub fn new(mut iter: T) -> Self {
    let item = iter.next();
    let next = iter.next();

    Self {
      first: item,
      item,
      next,
      iter,
    }
  }
}

impl<T> Iterator for PairwiseIter<T>
where
  T: Iterator,
  T::Item: Copy + Clone,
{
  type Item = (T::Item, T::Item);

  fn next(&mut self) -> Option<Self::Item> {
    match (self.item, self.next) {
      (None, None) => None,
      (None, Some(_)) => todo!(),
      (Some(item), None) => {
        let first = self.first.unwrap();

        self.item = None;
        self.next = None;

        Some((item, first))
      }
      (Some(item), Some(next)) => {
        self.item = Some(next);
        self.next = self.iter.next();

        Some((item, next))
      }
    }
  }
}
