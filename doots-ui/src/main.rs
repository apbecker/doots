use sauron::{html, Application, Cmd};

mod guitar;
mod piano;

pub fn main() {
  console_error_panic_hook::set_once();

  sauron::Program::mount_to_body(App::new());
}

pub struct App {
  guitar_is_selected: bool,
  guitar: guitar::App,
  piano: piano::App,
}

impl Application for App {
  type MSG = Msg;

  fn update(&mut self, msg: Msg) -> sauron::Cmd<Msg>
  where
    Self: Sized + 'static,
  {
    match msg {
      Msg::SelectGuitar => self.guitar_is_selected = true,
      Msg::SelectPiano => self.guitar_is_selected = false,
      Msg::Guitar(msg) => {
        self.guitar.update(msg);
      }
      Msg::Piano(msg) => {
        self.piano.update(msg);
      }
    }

    Cmd::none()
  }

  fn view(&self) -> sauron::Node<Msg> {
    let content = if self.guitar_is_selected {
      self.guitar.view().map_msg(Msg::Guitar)
    } else {
      self.piano.view().map_msg(Msg::Piano)
    };

    let content = html::div(
      [],
      [
        html::div(
          [],
          [
            html::button([sauron::on_click(|_| Msg::SelectGuitar)], [html::text("Guitar")]),
            html::button([sauron::on_click(|_| Msg::SelectPiano)], [html::text("Piano")]),
          ],
        ),
        html::div([], [content]),
      ],
    );

    html::main([], [content])
  }
}

impl App {
  pub fn new() -> Self {
    Self {
      guitar_is_selected: true,
      guitar: guitar::App::new(),
      piano: piano::App::new(),
    }
  }
}

#[derive(Clone, Copy)]
pub enum Msg {
  SelectGuitar,
  SelectPiano,
  Guitar(guitar::Msg),
  Piano(piano::Msg),
}
