use rand::Rng;

use crate::theory::{Mode, Note, Scale, Spn};

#[derive(Clone, Copy)]
pub struct Tuning([Spn; 6]);

impl Tuning {
  pub fn open_strings(&self) -> impl std::iter::Iterator<Item = Spn> {
    self.into_spns().into_iter()
  }

  pub fn open(&self, s: u8) -> Spn {
    assert!(s < 6);

    self.0[s as usize]
  }

  pub fn fret(&self, s: u8, f: u8) -> Spn {
    assert!(f < 24);

    self.open(s).add(f)
  }

  pub fn into_spns(self) -> [Spn; 6] {
    self.0
  }

  pub fn print_scale(self, scale: Scale) {
    let triad = [scale.note(0), scale.note(2), scale.note(4)];

    for n in 0..7 {
      print!(" {}", scale.note(n));
    }

    println!();
    println!("             *       *       *       *           :           *       *       *       *          ");
    println!("-o---1---2---3---4---5---6---7---8---9--10--11--12--13--14--15--16--17--18--19--20--21--22--23--");

    for open in self.into_spns() {
      for fret in 0..24 {
        let note = open.note().move_up(fret);

        let n = if !scale.contains(note) {
          FretNote::Regular(note)
        } else if triad.contains(&note) {
          FretNote::InChord(note)
        } else {
          FretNote::InScale(note)
        };

        if note.is_sharp() {
          print!("-{n}-")
        } else {
          print!("-{n}--")
        }
      }

      println!();
    }

    println!("-o---1---2---3---4---5---6---7---8---9--10--11--12--13--14--15--16--17--18--19--20--21--22--23--");
    println!("             *       *       *       *           :           *       *       *       *          ");
  }

  pub fn print_scales(self) {
    for &note in Note::values() {
      for &mode in Mode::values() {
        self.print_scale(Scale::heptatonic(note, mode));
      }
    }
  }

  /// Drop C tuning variation. (`CGCFAD`)
  #[rustfmt::skip]
  pub const fn drop_c_cgcfad() -> Tuning {
    Tuning([
      spn!(C, 2),
      spn!(G, 2),
      spn!(C, 3),
      spn!(F, 3),
      spn!(A, 3),
      spn!(D, 4),
    ])
  }

  /// Drop C tuning variation. (`CGDGBE`)
  #[rustfmt::skip]
  pub const fn drop_c_cgdgbe() -> Tuning {
    Tuning([
      spn!(C, 2),
      spn!(G, 2),
      spn!(D, 3),
      spn!(G, 3),
      spn!(B, 3),
      spn!(E, 4),
    ])
  }

  /// Drop C tuning. (`CADGBE`)
  #[rustfmt::skip]
  pub const fn drop_c_cadgbe() -> Tuning {
    Tuning([
      spn!(C, 2),
      spn!(A, 2),
      spn!(D, 3),
      spn!(G, 3),
      spn!(B, 3),
      spn!(E, 4),
    ])
  }

  /// Drop D tuning. (`DADGBE`)
  #[rustfmt::skip]
  pub const fn drop_d_dadgbe() -> Tuning {
    Tuning([
      spn!(D, 2),
      spn!(A, 2),
      spn!(D, 3),
      spn!(G, 3),
      spn!(B, 3),
      spn!(E, 4),
    ])
  }

  /// Open D tuning. (`DADGAD`)
  #[rustfmt::skip]
  pub const fn open_d_dadgad() -> Tuning {
    Tuning([
      spn!(D, 2),
      spn!(A, 2),
      spn!(D, 3),
      spn!(G, 3),
      spn!(A, 3),
      spn!(D, 4),
    ])
  }

  /// Fourths tuning. (`EADGCF`)
  #[rustfmt::skip]
  pub const fn fourths() -> Tuning {
    Tuning([
      spn!(E, 2),
      spn!(A, 2),
      spn!(D, 3),
      spn!(G, 3),
      spn!(C, 3),
      spn!(F, 4),
    ])
  }

  /// Standard tuning. (`EADGBE`)
  #[rustfmt::skip]
  pub const fn standard() -> Tuning {
    Tuning([
      spn!(E, 2),
      spn!(A, 2),
      spn!(D, 3),
      spn!(G, 3),
      spn!(B, 3),
      spn!(E, 4),
    ])
  }
}

pub fn print_random_strum() {
  println!("1 e & a 2 e & a 3 e & a 4 e & a");

  for strum in StrumPattern::random() {
    print!("{} ", strum);
  }

  println!();
}

enum_plus! {
  pub enum Strum {
    Rest,
    Down,
    Up,
  }
}

impl std::fmt::Display for Strum {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match self {
      Strum::Rest => write!(f, "{}", " "),
      Strum::Down => write!(f, "{}", "🠗"),
      Strum::Up => write!(f, "{}", "🠕"),
    }
  }
}

pub struct StrumPattern(Vec<Strum>);

impl StrumPattern {
  pub fn random() -> Self {
    let mut strums = vec![];
    let bar = rand::thread_rng().gen::<u16>();

    for n in 0..16 {
      let bit = (bar >> (15 - n)) & 1;

      if bit == 0 {
        strums.push(Strum::Rest);
      } else {
        match n & 1 {
          0 => strums.push(Strum::Down),
          _ => strums.push(Strum::Up),
        }
      }
    }

    Self(strums)
  }
}

impl std::iter::IntoIterator for StrumPattern {
  type Item = Strum;

  type IntoIter = std::vec::IntoIter<Self::Item>;

  fn into_iter(self) -> Self::IntoIter {
    self.0.into_iter()
  }
}

enum FretNote {
  InChord(Note),
  InScale(Note),
  Regular(Note),
}

impl std::fmt::Display for FretNote {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match self {
      FretNote::InChord(note) => {
        write!(f, "\x1b[1;4;34m{}\x1b[0;22m", note)
      }
      FretNote::InScale(note) => {
        write!(f, "\x1b[34m{}\x1b[0m", note)
      }
      FretNote::Regular(note) => {
        if note.is_sharp() {
          write!(f, "--")
        } else {
          write!(f, "-")
        }
      }
    }
  }
}
