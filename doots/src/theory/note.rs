enum_plus! {
  pub enum Note {
    C,
    CSharp,
    D,
    DSharp,
    E,
    F,
    FSharp,
    G,
    GSharp,
    A,
    ASharp,
    B,
  }
}

impl Note {
  pub const NATURAL: [Note; 7] = [Note::C, Note::D, Note::E, Note::F, Note::G, Note::A, Note::B];

  pub const fn move_down(self, semitones: u8) -> Self {
    let n = self.semitones();
    let n = if n < semitones { (n + 12) - semitones } else { n - semitones };

    Self::from_semitones(n)
  }

  pub const fn move_up(self, semitones: u8) -> Self {
    Self::from_semitones(self.semitones() + semitones)
  }

  pub const fn distance_down_to(self, note: Self) -> u8 {
    let root = self.semitones();
    let note = note.semitones();

    if root >= note {
      root - note
    } else {
      (root + 12) - note
    }
  }

  pub const fn distance_up_to(self, note: Self) -> u8 {
    let root = self.semitones();
    let note = note.semitones();

    if root > note {
      (note + 12) - root
    } else {
      note - root
    }
  }

  pub const fn from_semitones(val: u8) -> Self {
    Self::ALL[(val % 12) as usize]
  }

  /// How many semitones this note is above `C`.
  pub const fn semitones(self) -> u8 {
    match self {
      Note::C => 0,
      Note::CSharp => 1,
      Note::D => 2,
      Note::DSharp => 3,
      Note::E => 4,
      Note::F => 5,
      Note::FSharp => 6,
      Note::G => 7,
      Note::GSharp => 8,
      Note::A => 9,
      Note::ASharp => 10,
      Note::B => 11,
    }
  }

  pub const fn is_sharp(self) -> bool {
    match self {
      Note::CSharp | Note::DSharp | Note::FSharp | Note::GSharp | Note::ASharp => true,
      _ => false,
    }
  }
}

impl std::fmt::Display for Note {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    // D♭, E♭, G♭, A♭, B♭

    const NAMES: [&'static str; 12] = ["C", "C♯", "D", "D♯", "E", "F", "F♯", "G", "G♯", "A", "A♯", "B"];

    let s = NAMES[self.semitones() as usize];

    write!(f, "{}", s)
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn a_distance_up_to_b_equals_b_distance_down_to_a() {
    for a in Note::ALL {
      for b in Note::ALL {
        let distance_up_to = a.distance_up_to(b);
        let distance_down_to = b.distance_down_to(a);

        assert_eq!(distance_up_to, distance_down_to, "a={a}, b={b}")
      }
    }
  }
}
