use crate::intervals;

#[derive(Clone, Copy, Debug)]
pub struct IntervalSet(u32);

impl IntervalSet {
  #[rustfmt::skip]
  pub const ALL: IntervalSet = intervals!(
    P1, MIN2, MAJ2, MIN3, MAJ3,
    P4, TRITONE,
    P5, MIN6, MAJ6, MIN7, MAJ7,
    P8
  );

  pub fn new(vals: &[Interval]) -> Self {
    let vals = vals.iter().map(|&i| Self::bit_flag(i)).fold(0, std::ops::BitOr::bitor);

    Self(vals)
  }

  pub const fn from_u32(vals: u32) -> Self {
    Self(vals)
  }

  pub const fn has(&self, interval: Interval) -> bool {
    (self.0 & Self::bit_flag(interval)) != 0
  }

  pub fn matches_without_extensions(&self, other: Self) -> bool {
    (self.0 & Self::ALL.0) == (other.0 & Self::ALL.0)
  }

  const fn bit_flag(interval: Interval) -> u32 {
    1 << (interval as u8)
  }
}

impl std::fmt::Display for IntervalSet {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{{")?;

    let mut extra = false;

    for interval in 0..12 {
      if (self.0 & (1 << interval)) == 0 {
        continue;
      }

      if extra {
        write!(f, ", ")?;
      }

      extra = true;

      write!(f, "{}", interval)?;
    }

    write!(f, "}}")
  }
}

pub struct HalfStep(u32);

impl HalfStep {
  pub const fn new(val: u8) -> Self {
    Self(1 << val)
  }

  pub const fn bit(self) -> u32 {
    self.0
  }
}

enum_plus! {
  pub enum Interval {
    Unison,
    Minor2,
    Major2,
    Minor3,
    Major3,
    Perfect4,
    Tritone,
    Perfect5,
    Minor6,
    Major6,
    Minor7,
    Major7,
  }
}

impl From<u8> for Interval {
  fn from(value: u8) -> Self {
    match value % 12 {
      1 => Self::Minor2,
      2 => Self::Major2,
      3 => Self::Minor3,
      4 => Self::Major3,
      5 => Self::Perfect4,
      6 => Self::Tritone,
      7 => Self::Perfect5,
      8 => Self::Minor6,
      9 => Self::Major6,
      10 => Self::Minor7,
      11 => Self::Major7,
      _ => Self::Unison,
    }
  }
}

/// The perfect unison.
pub const P1: HalfStep = HalfStep::new(0);

/// The minor 2nd.
pub const MIN2: HalfStep = HalfStep::new(1);

/// The major 2nd.
pub const MAJ2: HalfStep = HalfStep::new(2);

/// The minor 3rd.
pub const MIN3: HalfStep = HalfStep::new(3);

/// The major 3rd.
pub const MAJ3: HalfStep = HalfStep::new(4);

/// The perfect 4th.
pub const P4: HalfStep = HalfStep::new(5);

/// The tritone.
pub const TRITONE: HalfStep = HalfStep::new(6);

/// The tritone.
pub const A4: HalfStep = TRITONE;

/// The tritone.
pub const D5: HalfStep = TRITONE;

/// The perfect 5th.
pub const P5: HalfStep = HalfStep::new(7);

/// The minor 6th.
pub const MIN6: HalfStep = HalfStep::new(8);

/// The major 6th.
pub const MAJ6: HalfStep = HalfStep::new(9);

/// The minor 7th.
pub const MIN7: HalfStep = HalfStep::new(10);

/// The major 7th.
pub const MAJ7: HalfStep = HalfStep::new(11);

/// The perfect octave.
pub const P8: HalfStep = HalfStep::new(12);
