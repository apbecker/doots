use crate::theory::Note;

use super::IntervalSet;

#[derive(Clone, Copy)]
pub struct ScaleDegree(usize);

impl ScaleDegree {
  pub fn into_index(self) -> usize {
    self.0 - 1
  }
}

#[derive(Debug)]
pub struct Scale {
  notes: Vec<Note>,
}

impl Scale {
  pub fn new(notes: &[Note]) -> Self {
    Self { notes: notes.to_vec() }
  }

  pub fn contains(&self, note: Note) -> bool {
    self.notes.contains(&note)
  }

  pub fn root(&self) -> Note {
    self.note(0)
  }

  pub fn note(&self, n: usize) -> Note {
    self.notes[n % self.notes.len()]
  }

  pub fn chromatic() -> Self {
    Self::new(&Note::ALL)
  }

  /// Creates a 5 note scale. Panics if `mode` is anything besides `Ionian` or `Aeolian`.
  pub fn pentatonic(root: Note, mode: Mode) -> Self {
    let notes = scale_notes(root, mode);
    let degrees = match mode {
      Mode::Ionian | Mode::Lydian | Mode::Mixolydian => [1, 2, 3, 5, 6],
      Mode::Dorian | Mode::Phrygian | Mode::Aeolian => [1, 3, 4, 5, 7],
      _ => panic!(""),
    };

    Self::new(&degrees.map(ScaleDegree).map(|d| notes[d.into_index()]))
  }

  /// Creates a 7 note scale.
  pub fn heptatonic(root: Note, mode: Mode) -> Scale {
    Scale::new(&scale_notes(root, mode))
  }
}

const LYDIAN: IntervalSet = intervals!(P1, MAJ2, MAJ3, A4, P5, MAJ6, MAJ7);
const IONIAN: IntervalSet = intervals!(P1, MAJ2, MAJ3, P4, P5, MAJ6, MAJ7);
const MIXOLYDIAN: IntervalSet = intervals!(P1, MAJ2, MAJ3, P4, P5, MAJ6, MIN7);
const DORIAN: IntervalSet = intervals!(P1, MAJ2, MIN3, P4, P5, MAJ6, MIN7);
const AEOLIAN: IntervalSet = intervals!(P1, MAJ2, MIN3, P4, P5, MIN6, MIN7);
const PHRYGIAN: IntervalSet = intervals!(P1, MIN2, MIN3, P4, P5, MIN6, MIN7);
const LOCRIAN: IntervalSet = intervals!(P1, MIN2, MIN3, P4, D5, MIN6, MIN7);

#[rustfmt::skip]
fn scale_notes(root: Note, mode: Mode) -> [Note; 7] {
  let notes = [0, 2, 4, 5, 7, 9, 11].map(|n| root.move_up(n));

  match mode {
    Mode::Lydian     => pick(notes, " 1, 2, 3,♯4, 5, 6, 7"),
    Mode::Ionian     => pick(notes, " 1, 2, 3, 4, 5, 6, 7"),
    Mode::Mixolydian => pick(notes, " 1, 2, 3, 4, 5, 6,♭7"),
    Mode::Dorian     => pick(notes, " 1, 2,♭3, 4, 5, 6,♭7"),
    Mode::Aeolian    => pick(notes, " 1, 2,♭3, 4, 5,♭6,♭7"),
    Mode::Phrygian   => pick(notes, " 1,♭2,♭3, 4, 5,♭6,♭7"),
    Mode::Locrian    => pick(notes, " 1,♭2,♭3, 4,♭5,♭6,♭7"),
  }
}

fn pick(notes: [Note; 7], s: &'static str) -> [Note; 7] {
  let mut notes = notes.clone();

  for s in s.split(",") {
    let mut chars = s.chars();

    let accidental = chars.next().expect("missing accidental");
    let degree = chars.next().expect("missing degree");
    let degree = degree.to_digit(10).expect("degree must be a number");
    let degree = ScaleDegree(degree as usize);
    let index = degree.into_index();

    match accidental {
      ' ' => {}
      '♭' => notes[index] = notes[index].move_down(1),
      '♯' => notes[index] = notes[index].move_up(1),
      _ => todo!("{accidental:?}"),
    }
  }

  notes
}

enum_plus! {
  pub enum Mode {
    Ionian,
    Dorian,
    Phrygian,
    Lydian,
    Mixolydian,
    Aeolian,
    Locrian,
  }
}

impl Mode {
  pub const fn major() -> Self {
    Self::Ionian
  }

  pub const fn minor() -> Self {
    Self::Aeolian
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn heptatonic_scale_works() {
    let scale = Scale::heptatonic(Note::E, Mode::major());

    assert_eq!(Note::E, scale.note(0));
    assert_eq!(Note::FSharp, scale.note(1));
    assert_eq!(Note::GSharp, scale.note(2));
    assert_eq!(Note::A, scale.note(3));
    assert_eq!(Note::B, scale.note(4));
    assert_eq!(Note::CSharp, scale.note(5));
    assert_eq!(Note::DSharp, scale.note(6));

    let scale = Scale::heptatonic(Note::E, Mode::minor());

    assert_eq!(Note::E, scale.note(0));
    assert_eq!(Note::FSharp, scale.note(1));
    assert_eq!(Note::G, scale.note(2));
    assert_eq!(Note::A, scale.note(3));
    assert_eq!(Note::B, scale.note(4));
    assert_eq!(Note::C, scale.note(5));
    assert_eq!(Note::D, scale.note(6));
  }

  #[test]
  fn pentatonic_scale_works() {
    let scale = Scale::pentatonic(Note::E, Mode::major());

    assert_eq!(Note::E, scale.note(0));
    assert_eq!(Note::FSharp, scale.note(1));
    assert_eq!(Note::GSharp, scale.note(2));
    assert_eq!(Note::B, scale.note(3));
    assert_eq!(Note::CSharp, scale.note(4));

    let scale = Scale::pentatonic(Note::E, Mode::minor());

    assert_eq!(Note::E, scale.note(0));
    assert_eq!(Note::G, scale.note(1));
    assert_eq!(Note::A, scale.note(2));
    assert_eq!(Note::B, scale.note(3));
    assert_eq!(Note::D, scale.note(4));
  }
}
