use std::collections::HashSet;

use crate::theory::interval::{Interval, IntervalSet};
use crate::theory::Note;

#[derive(Clone, Copy, Debug)]
pub struct Chord {
  root: Note,
  intervals: IntervalSet,
}

impl Chord {
  pub fn new(root: Note, notes: &HashSet<Note>) -> Self {
    let intervals = notes
      .iter()
      .map(|&e| root.distance_up_to(e))
      .map(Interval::from)
      .collect::<Vec<_>>();

    Self {
      root,
      intervals: IntervalSet::new(&intervals),
    }
  }

  pub fn search(notes: &HashSet<Note>) -> Vec<(Note, NamedChord)> {
    let mut known_chords = vec![];

    for root in Note::ALL {
      if !notes.contains(&root) {
        continue;
      }

      let chord = Self::new(root, notes);

      for known_chord in NamedChord::ALL.iter() {
        if known_chord.intervals().matches_without_extensions(chord.intervals) {
          known_chords.push((root, known_chord.clone()))
        }
      }
    }

    known_chords
  }
}

impl std::fmt::Display for Chord {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{} {}", self.root, self.intervals)
  }
}

#[derive(Clone, Debug)]
pub struct NamedChord(&'static str, IntervalSet);

impl NamedChord {
  pub const ALL: &'static [NamedChord] = &[
    NamedChord("aug", intervals!(P1, MAJ3, MIN6)),
    NamedChord("aug(m7)", intervals!(P1, MAJ3, MIN6, MIN7)),
    NamedChord("dim", intervals!(P1, MIN3, D5)),
    NamedChord("dim(d7)", intervals!(P1, MIN3, D5, MAJ6)),
    NamedChord("dim(m7)", intervals!(P1, MIN3, D5, MIN7)),
    NamedChord("maj", intervals!(P1, MAJ3, P5)),
    NamedChord("maj(M6)", intervals!(P1, MAJ3, P5, MAJ6)),
    NamedChord("maj(m7)", intervals!(P1, MAJ3, P5, MIN7)),
    NamedChord("maj(M7)", intervals!(P1, MAJ3, P5, MAJ7)),
    NamedChord("min", intervals!(P1, MIN3, P5)),
    NamedChord("min(m6)", intervals!(P1, MIN3, P5, MIN6)),
    NamedChord("min(M6)", intervals!(P1, MIN3, P5, MAJ6)),
    NamedChord("min(m7)", intervals!(P1, MIN3, P5, MIN7)),
    NamedChord("5", intervals!(P1, P5)),
    NamedChord("sus2", intervals!(P1, MAJ2, P5)),
    NamedChord("sus4", intervals!(P1, P4, P5)),
  ];

  pub fn suffix(&self) -> &'static str {
    self.0
  }

  pub fn intervals(&self) -> IntervalSet {
    self.1
  }
}
