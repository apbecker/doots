use super::Note;

#[derive(Clone, Copy, Debug, Default)]
pub struct Frequency(f64);

impl Frequency {
  pub fn into_inner(self) -> f64 {
    self.0
  }
}

#[derive(Clone, Copy, Debug)]
pub struct Spn {
  octave: u8,
  note: Note,
}

impl Spn {
  pub const fn new(note: Note, octave: u8) -> Self {
    Self { note, octave }
  }

  pub const fn note(&self) -> Note {
    self.note
  }

  pub const fn octave(&self) -> u8 {
    self.octave
  }

  pub fn add(self, semitones: u8) -> Spn {
    let n = self.into_u8();

    Self::from_u8(n + semitones)
  }

  pub fn sub(self, semitones: u8) -> Spn {
    let n = self.into_u8();

    Self::from_u8(n - semitones)
  }

  pub const fn from_u8(val: u8) -> Self {
    let octave = val / 12;
    let note = Note::from_semitones(val % 12);

    Self::new(note, octave)
  }

  pub(crate) fn into_u8(self) -> u8 {
    let o = self.octave();
    let n = self.note().semitones();

    (o * 12) + n
  }

  pub fn into_frequency(self) -> Frequency {
    let n = self.into_u8() + 3;
    let f = frequency_curve(n as f64);

    Frequency(f)
  }
}

pub(crate) fn frequency_curve(x: f64) -> f64 {
  /// A above middle C.
  const CONCERT_A: f64 = 440.0;

  CONCERT_A * f64::powf(2.0, (x / 12.0) - 5.0)
}

impl Default for Spn {
  fn default() -> Self {
    Self::new(Note::C, 0)
  }
}

impl std::fmt::Display for Spn {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}{}", self.note, self.octave)
  }
}

#[cfg(test)]
pub mod tests {
  use super::*;

  #[test]
  fn into_hertz() {
    let frequencies = [
      //     C         C#        D         D#        E         F         F#        G         G#        A         A#        B
      16.35160, 17.32391, 18.35405, 19.44544, 20.60172, 21.82676, 23.12465, 24.49971, 25.95654, 27.50000, 29.13524, 30.86771,
      32.70320, 34.64783, 36.70810, 38.89087, 41.20344, 43.65353, 46.24930, 48.99943, 51.91309, 55.00000, 58.27047, 61.73541,
      65.40639, 69.29566, 73.41619, 77.78175, 82.40689, 87.30706, 92.49861, 97.99886, 103.8262, 110.0000, 116.5409, 123.4708,
      130.8128, 138.5913, 146.8324, 155.5635, 164.8138, 174.6141, 184.9972, 195.9977, 207.6523, 220.0000, 233.0819, 246.9417,
      261.6256, 277.1826, 293.6648, 311.1270, 329.6276, 349.2282, 369.9944, 391.9954, 415.3047, 440.0000, 466.1638, 493.8833,
      523.2511, 554.3653, 587.3295, 622.2540, 659.2551, 698.4565, 739.9888, 783.9909, 830.6094, 880.0000, 932.3275, 987.7666,
      1046.502, 1108.731, 1174.659, 1244.508, 1318.510, 1396.913, 1479.978, 1567.982, 1661.219, 1760.000, 1864.655, 1975.533,
      2093.005, 2217.461, 2349.318, 2489.016, 2637.020, 2793.826, 2959.955, 3135.963, 3322.438, 3520.000, 3729.310, 3951.066,
      4186.009, 4434.922, 4698.636, 4978.032, 5274.041, 5587.652, 5919.911, 6271.927, 6644.875, 7040.000, 7458.620, 7902.133,
      8372.018, 8869.844, 9397.273, 9956.063, 10548.08, 11175.30, 11839.82, 12543.85, 13289.75, 14080.00, 14917.24, 15804.27,
      16744.04, 17739.69, 18794.55, 19912.13, 21096.16, 22350.61, 23679.64, 25087.71, 26579.50, 28160.00, 29834.48, 31608.53,
    ];

    for (k, frequency) in frequencies.iter().enumerate() {
      let spn = {
        let k = k as u8;
        let octave = k / 12;
        let note = Note::from_semitones(k % 12);
        Spn::new(note, octave)
      };

      let expected = frequency;
      let actual = spn.into_frequency().into_inner();
      let diff = (expected - actual).abs();

      // within a cent
      assert!(diff < 0.01, "k={}, expected={}, actual={}", k, frequency, actual);
    }
  }
}
