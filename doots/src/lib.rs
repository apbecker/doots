macro_rules! enum_plus {
  (pub enum $name:ident { $($variant:ident,)* }) => {
    #[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
    pub enum $name {
      $($variant),*
    }

    impl $name {
      pub const MEMBERS: usize = enum_plus!(@count, 0, $($variant),*);

      pub const ALL: [$name; Self::MEMBERS] = [
        $($name::$variant),*
      ];

      pub fn values() -> &'static [$name] {
        &Self::ALL
      }
    }
  };

  (@count, $acc:expr) => { $acc };
  (@count, $acc:expr, $val:ident $(, $vals:ident)*) => {
    enum_plus!(@count, $acc + 1 $(, $vals)*)
  };
}

#[macro_export]
macro_rules! intervals {
  ($($vals:ident),*) => {
    crate::theory::interval::IntervalSet::from_u32(intervals!(@merge, 0, $($vals),*))
  };

  (@merge, $acc:expr) => { $acc };
  (@merge, $acc:expr, $val:ident $(, $vals:ident)*) => {
    intervals!(@merge, $acc | crate::theory::interval::$val.bit() $(, $vals)*)
  };
}

#[macro_export]
macro_rules! spn {
  ($note:ident, $octave:literal) => {{
    let note = crate::theory::Note::$note;
    let octave = $octave;
    crate::theory::Spn::new(note, octave)
  }};
}

pub mod guitar;
pub mod synth;
pub mod theory;
