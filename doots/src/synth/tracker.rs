use super::{ks, wave::WaveTable};

use crate::theory::Spn;

#[derive(Clone)]
pub struct Track {
  pub beats_per_minute: u32,
  pub beats_per_whole: u32,
  pub instrument: Instrument,
  pub sounds: Vec<(Duration, Sound)>,
  pub volume: f64,
}

impl Track {
  pub fn to_samples(&self, samples_per_second: u32, samples_per_whole: u32) -> Vec<f32> {
    let mut samples = vec![];

    for &(duration, sound) in &self.sounds {
      let count = duration.scale(samples_per_whole) as usize;

      match sound {
        Sound::Rest => {
          let s = vec![0.0; count];
          samples.extend_from_slice(&s[..])
        }
        Sound::Note(_, spn) => {
          let mut s = vec![0.0; count];

          self.instrument.play(spn, samples_per_second, &mut s);

          samples.extend_from_slice(&s[..])
        }
      }
    }

    samples
  }
}

#[derive(Clone)]
pub enum Instrument {
  Guitar,
  Wave(WaveTable),
}

impl Instrument {
  pub fn play(&self, spn: Spn, sample_rate: u32, samples: &mut [f32]) {
    match self {
      Self::Guitar => {
        let f = spn.into_frequency().into_inner().round() as u32;
        ks::string(f, sample_rate, samples)
      }
      Self::Wave(table) => {
        for sample in samples {
          *sample = table.sample(0) as f32;
        }
      }
    }
  }
}

#[derive(Clone, Copy, Debug)]
pub enum Sound {
  Rest,
  Note(f64, Spn),
}

#[derive(Clone, Copy, Debug)]
pub struct Duration(u32);

impl Duration {
  pub const W: Self = Self(16);
  pub const H: Self = Self(8);
  pub const Q: Self = Self(4);
  pub const E: Self = Self(2);
  pub const S: Self = Self(1);

  pub fn from_sixteenths(val: u32) -> Self {
    Self(val)
  }

  pub fn into_sixteenths(self) -> u32 {
    self.0
  }

  pub fn dotted(self) -> Self {
    let n = self.0;
    let n = n + (n / 2);
    Self(n)
  }

  pub fn scale(self, val: u32) -> u32 {
    (val * self.into_sixteenths()) / 16
  }
}
