use std::f64::consts::PI;

#[derive(Clone)]
pub struct WaveTable {
  samples: Vec<f64>,
}

const HARMONICS: u32 = 20;

impl WaveTable {
  pub fn noise(sample_rate: u32) -> Self {
    Self::from_parts(sample_rate, noise_wave)
  }

  pub fn sawtooth(sample_rate: u32) -> Self {
    Self::from_parts(sample_rate, sawtooth_wave)
  }

  pub fn sine(sample_rate: u32) -> Self {
    Self::from_parts(sample_rate, sine_wave)
  }

  pub fn square(sample_rate: u32) -> Self {
    Self::from_parts(sample_rate, square_wave)
  }

  pub fn triangle(sample_rate: u32) -> Self {
    Self::from_parts(sample_rate, triangle_wave)
  }

  pub fn from_parts(sample_rate: u32, callback: impl Fn(f64) -> f64) -> Self {
    let mut samples = vec![];

    for sample in 0..sample_rate {
      let sample = callback((sample as f64) / (sample_rate as f64));
      samples.push(sample)
    }

    Self::from_vec(samples)
  }

  pub fn from_vec(samples: Vec<f64>) -> Self {
    Self { samples }
  }

  pub fn sample(&self, n: usize) -> f64 {
    self.samples[n % self.samples.len()]
  }
}

fn noise_wave(_: f64) -> f64 {
  use rand::Rng;

  rand::thread_rng().gen_range(0.0..=1.0)
}

fn sawtooth_wave(t: f64) -> f64 {
  let mut sample = 0.0;

  for k in 0..HARMONICS {
    let n = (k + 1) as f64;

    if (k % 2) == 0 {
      sample += wave(n, t) / n;
    } else {
      sample -= wave(n, t) / n;
    }
  }

  sample * (1.0 / PI)
}

fn sine_wave(t: f64) -> f64 {
  wave(1.0, t)
}

fn square_wave(t: f64) -> f64 {
  let mut sample = 0.0;

  for k in 0..HARMONICS {
    let n = (2 * k + 1) as f64;

    sample += wave(n, t) / n;
  }

  sample * (2.0 / PI)
}

fn triangle_wave(t: f64) -> f64 {
  let mut sample = 0.0;

  for k in 0..HARMONICS {
    let n = (2 * k + 1) as f64;

    if (k % 2) == 0 {
      sample += wave(n, t) / (n * n)
    } else {
      sample -= wave(n, t) / (n * n)
    }
  }

  sample * (4.0 / (PI * PI))
}

fn wave(n: f64, t: f64) -> f64 {
  (2.0 * PI * t * n).sin()
}
