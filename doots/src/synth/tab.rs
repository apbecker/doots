use std::path::Path;

use crate::synth::tracker::{Duration, Sound};

pub struct TabString {
  sounds: Vec<(Duration, Sound)>,
}

impl TabString {
  pub fn into_sounds(self) -> Vec<(Duration, Sound)> {
    self.sounds
  }
}

pub struct Tab {
  pub capo: u8,
  pub tempo: u32,
  pub time: String,
  pub strings: Vec<TabString>,
}

impl Tab {
  pub fn from_file(p: impl AsRef<Path>) -> Self {
    let s = std::fs::read_to_string(p).unwrap();

    Self::from_str(&s)
  }

  pub fn from_str(s: &str) -> Self {
    let mut lines = s.lines();
    let props = parse_properties(&mut lines);

    let _beats = lines.next().unwrap();
    let rhythm_line = lines.next().unwrap();
    let rhythm = parse_rhythm_line(&rhythm_line).unwrap();
    let tuning = crate::guitar::Tuning::drop_d_dadgbe().into_spns();
    let mut strings = vec![];

    for (n, string) in lines.take(6).enumerate() {
      let open = tuning[n].add(props.capo);
      let mut sounds = vec![];
      let mut last_note_index = 0;

      for &(n, duration) in &rhythm {
        let s = &string[n..];

        if s.starts_with("-L-") {
          sounds.push((duration, Sound::Rest));

          let duration = Duration::from_sixteenths(sounds[last_note_index..].iter().map(|(d, _)| d.into_sixteenths()).sum());

          let (_, sound) = sounds[last_note_index];

          sounds.truncate(last_note_index);
          sounds.push((duration, sound));

          continue;
        }

        let result = if s.starts_with('-') {
          u8::from_str_radix(&s[1..2], 10)
        } else {
          u8::from_str_radix(&s[0..2], 10)
        };

        let sound = if let Ok(fret) = result {
          last_note_index = sounds.len();
          Sound::Note(1.0, open.add(fret))
        } else {
          Sound::Rest
        };

        sounds.push((duration, sound));
      }

      strings.push(TabString { sounds })
    }

    Tab {
      capo: props.capo,
      tempo: props.tempo,
      time: props.time,
      strings,
    }
  }
}

fn parse_properties(lines: &mut std::str::Lines<'_>) -> Properties {
  let mut capo = None;
  let mut tempo = None;
  let mut time = None;

  while let Some(line) = lines.next() {
    if line.is_empty() {
      break;
    }

    match <Vec<_> as TryInto<[_; 2]>>::try_into(line.split(": ").collect()) {
      Ok(["Capo", value]) => capo = Some(u8::from_str_radix(value, 10).unwrap()),
      Ok(["Tempo", value]) => tempo = Some(u32::from_str_radix(value, 10).unwrap()),
      Ok(["Time", value]) => time = Some(value.to_string()),
      Ok([name, value]) => println!("unexpected property: name={name}, value={value}"),
      Err(_) => panic!(),
    }
  }

  Properties {
    capo: capo.unwrap_or(0),
    tempo: tempo.unwrap(),
    time: time.unwrap(),
  }
}

fn parse_rhythm_line(line: &str) -> Option<Vec<(usize, Duration)>> {
  const COL_WIDTH: usize = 3;

  let pattern = regex::Regex::new(" (?P<duration>[SEQHW])(?P<augment>[ .])").ok()?;
  let columns = {
    let n = line.len();
    (0..n).step_by(COL_WIDTH).skip(1)
  };

  let mut durations = vec![];

  for column in columns {
    let Some(captures) = pattern.captures(&line[column..column + COL_WIDTH]) else {
      // not a show-stopper, could be an empty column
      continue;
    };

    let duration = &captures["duration"];
    let augment = &captures["augment"];

    let mut duration = match duration {
      "S" => Duration::S,
      "E" => Duration::E,
      "Q" => Duration::Q,
      "H" => Duration::H,
      "W" => Duration::W,
      _ => continue,
    };

    if augment == "." {
      duration = duration.dotted();
    }

    durations.push((column, duration));
  }

  Some(durations)
}

#[allow(dead_code)]
fn parse_beat_line(s: &str) -> Option<Vec<()>> {
  assert_eq!("   ", &s[0..3]);

  let mut start = None;

  for n in (3..s.len() - 1).step_by(3) {
    match &s[n..n + 3] {
      "   " => {} // for formatting purposes
      " - " => {
        assert!(start.is_none());
        println!("start: {n}, end: {n}");
      }
      " --" => {
        assert!(start.is_none());
        start = Some(n);
      }
      "---" => {
        assert!(start.is_some());
      }
      "-- " => {
        let start = start.take()?;
        println!("start: {start}, end: {n}");
      }
      s => panic!("unhandled: {s}"),
    }
  }

  Some(vec![])
}

struct Properties {
  capo: u8,
  tempo: u32,
  #[allow(dead_code)]
  time: String,
}
