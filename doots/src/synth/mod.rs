mod envelope;
mod ks;
mod player;
mod tab;
mod tracker;
mod wave;

use player::Player;
use tab::Tab;
use tracker::{Instrument, Track};
use wave::WaveTable;

pub fn play_tabs(tabs: &[String], samples_per_second: u32) {
  let mut tracks = vec![];

  let instrument = [
    Instrument::Guitar,
    Instrument::Wave(WaveTable::noise(samples_per_second)),
    Instrument::Wave(WaveTable::sawtooth(samples_per_second)),
    Instrument::Wave(WaveTable::sine(samples_per_second)),
    Instrument::Wave(WaveTable::square(samples_per_second)),
    Instrument::Wave(WaveTable::triangle(samples_per_second)),
  ];

  for tab in tabs {
    let tab = Tab::from_file(tab);

    for string in tab.strings {
      tracks.push(Track {
        beats_per_minute: tab.tempo,
        beats_per_whole: 4, // TODO: replace with values from time signature.
        instrument: instrument[0].clone(),
        volume: 1.0,
        sounds: string.into_sounds(),
      })
    }
  }

  Player::play_song(samples_per_second, &tracks).unwrap()
}
