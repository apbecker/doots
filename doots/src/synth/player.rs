use std::{fs::File, io::BufWriter};

use hound::{SampleFormat, WavSpec, WavWriter};

use crate::synth::tracker::Track;

pub struct Player {
  writer: WavWriter<BufWriter<File>>,
}

impl Player {
  pub fn new(samples_per_second: u32) -> Self {
    let spec = WavSpec {
      channels: 2,
      sample_rate: samples_per_second,
      bits_per_sample: 32,
      sample_format: SampleFormat::Float,
    };

    Self {
      writer: WavWriter::create("out.wav", spec).unwrap(),
    }
  }

  pub fn play_song(samples_per_second: u32, tracks: &[Track]) -> anyhow::Result<()> {
    if tracks.is_empty() {
      return Ok(());
    }

    let mut samples_for_all_tracks = vec![];
    let mut samples_for_longest_track = 0;

    for track in tracks {
      let seconds_per_minute = 60.0;
      let beats_per_second = (track.beats_per_minute as f64) / seconds_per_minute;
      let samples_per_beat = (samples_per_second as f64) / beats_per_second;
      let beats_per_whole = track.beats_per_whole as f64;
      let samples_per_whole = (samples_per_beat * beats_per_whole) as u32;
      let samples_for_track = track.to_samples(samples_per_second, samples_per_whole);

      samples_for_longest_track = samples_for_longest_track.max(samples_for_track.len());
      samples_for_all_tracks.push(samples_for_track);
    }

    let mut player = Self::new(samples_per_second);

    for n in 0..samples_for_longest_track {
      let mut sample = 0.0;

      for samples_for_track in &samples_for_all_tracks {
        if n < samples_for_track.len() {
          sample += samples_for_track[n];
        }
      }

      sample /= tracks.len() as f32;

      player.writer.write_sample(sample)?;
      player.writer.write_sample(sample)?;
    }

    Ok(())
  }
}
