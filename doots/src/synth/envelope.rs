#[derive(Clone)]
pub struct Envelope(Vec<(f64, f64)>);

#[allow(dead_code)]
impl Envelope {
  pub fn new(points: &[(f64, f64)]) -> Self {
    Self(points.iter().cloned().collect())
  }

  pub fn value(&self, d: f64) -> f64 {
    let k = self.0.len();

    for n in 0..k - 1 {
      let (t0, v0) = self.0[n];
      let (t1, v1) = self.0[n + 1];

      if t0 <= d && d < t1 {
        let mid = (d - t0) / (t1 - t0);

        return (v1 * mid) + (v0 * (1.0 - mid));
      }
    }

    0.0
  }
}
