use rand::Rng;

pub fn string(frequency: u32, sample_rate: u32, samples: &mut [f32]) {
  let mut string = KarplusStrong::new(frequency, sample_rate);

  for cycle_start in (0..samples.len()).step_by(string.cycle_size()) {
    let cycle_end = cycle_start + string.cycle_size();
    let cycle_end = cycle_end.min(samples.len());

    string.cycle(&mut samples[cycle_start..cycle_end]);
  }
}

pub struct KarplusStrong {
  /// Ring buffer with capacity for two cycles of the string.
  delay_line: Vec<f32>,
  /// The number of samples in a single cycle.
  cycle_size: usize,
  /// The address of the sample being read.
  read_base: usize,
  /// The address of the sample being written.
  write_base: usize,
}

impl KarplusStrong {
  pub fn new(frequency: u32, sample_rate: u32) -> Self {
    let length = (sample_rate / frequency) as usize;
    let mut samples = vec![0.0; length * 2];
    let mut filter = LowPassFilter::new(0.5);

    for i in 0..length {
      samples[i] = rand::thread_rng().gen_range(-1.0..1.0) * 0.5;
    }

    for i in 0..length {
      samples[i] = filter.apply(samples[i]);
    }

    Self {
      delay_line: samples,
      cycle_size: length,
      read_base: 0,
      write_base: length,
    }
  }

  pub fn cycle_size(&self) -> usize {
    self.cycle_size
  }

  fn cycle(&mut self, buf: &mut [f32]) {
    let k = self.cycle_size();

    for n in 0..buf.len() {
      let m = if n == 0 { k } else { n };
      let x = self.delay_line[self.read_base + n];
      let y = self.delay_line[self.read_base + m - 1];
      let s = (x + y) * 0.5;

      self.delay_line[self.write_base + n] = s;

      buf[n] = s;
    }

    self.read_base = self.cycle_size - self.read_base;
    self.write_base = self.cycle_size - self.write_base;
  }
}

pub struct LowPassFilter {
  a: f32,
  x_1: f32,
  y_1: f32,
}

impl LowPassFilter {
  pub fn new(a: f32) -> Self {
    Self { a, x_1: 0.0, y_1: 0.0 }
  }

  pub fn apply(&mut self, x: f32) -> f32 {
    let a = self.a;
    let x_1 = self.x_1;
    let y_1 = self.y_1;
    let y = (1.0 - a) * y_1 + (a * (x + x_1));

    self.x_1 = x;
    self.y_1 = y;

    y
  }
}
