#![allow(dead_code)]

use doots::guitar::Tuning;
use doots::theory::{Mode, Note, Scale, Spn};

fn main() {
  // chord_search();

  // let scale = Scale::chromatic();
  // let scale = Scale::custom(&[Note::C, Note::E, Note::G]);

  let scale = Scale::heptatonic(Note::C, Mode::Ionian);
  let tuning = Tuning::standard();

  println!("   0     1     2     3     4     5     6     7     8     9     0     1     2");

  for open in tuning.open_strings() {
    print_string_scale(&scale, open);
  }

  println!("   0     1     2     3     4     5     6     7     8     9     0     1     2");

  let tabs = std::env::args().skip(1).collect::<Vec<_>>();

  doots::synth::play_tabs(&tabs, 44100);
}

fn print_string_scale(scale: &Scale, open: Spn) {
  let mut notes = vec![];

  for fret in 0..=12 {
    let spn = open.add(fret);
    let note = spn.note();
    let value = if !scale.contains(note) {
      format!("---")
    } else {
      format!("-{:-<2}", note.to_string())
    };

    notes.push(value);
  }

  println!("|-{}-|", notes.join("-|-"));
}
