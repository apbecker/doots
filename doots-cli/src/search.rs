pub fn chord_search() {
  let notes = HashSet::from([
    Note::C, //
    Note::E, //
    Note::G, //
    Note::B, //
  ]);

  println!("notes:");
  println!();

  for note in &notes {
    println!("* {}", note);
  }

  println!();
  println!("names:");
  println!();

  for (root, known_chord) in Chord::search(&notes) {
    println!("* {}{} - {}", root, known_chord.suffix(), known_chord.intervals());
  }
}
